﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="21008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Day 01" Type="Folder">
			<Item Name="Day 01.vi" Type="VI" URL="../Day 01/Day 01.vi"/>
		</Item>
		<Item Name="Day 02" Type="Folder">
			<Item Name="Day 02.vi" Type="VI" URL="../Day 02/Day 02.vi"/>
		</Item>
		<Item Name="Day 03" Type="Folder">
			<Item Name="SubVIs" Type="Folder"/>
			<Item Name="Day 03.vi" Type="VI" URL="../Day 03/Day 03.vi"/>
		</Item>
		<Item Name="Day 04" Type="Folder">
			<Item Name="Day 04 - Create Set.vi" Type="VI" URL="../Day 04/Day 04 - Create Set.vi"/>
			<Item Name="Day 04.vi" Type="VI" URL="../Day 04/Day 04.vi"/>
		</Item>
		<Item Name="Day 05" Type="Folder">
			<Item Name="Day 05.vi" Type="VI" URL="../Day 05/Day 05.vi"/>
		</Item>
		<Item Name="Day 06" Type="Folder">
			<Item Name="Day 06.vi" Type="VI" URL="../Day 06/Day 06.vi"/>
		</Item>
		<Item Name="Day 07" Type="Folder">
			<Item Name="Day 07.vi" Type="VI" URL="../Day 07/Day 07.vi"/>
		</Item>
		<Item Name="Day 08" Type="Folder">
			<Item Name="Day 08 - Calculate Scenic Distance.vi" Type="VI" URL="../Day 08/Day 08 - Calculate Scenic Distance.vi"/>
			<Item Name="Day 08 - Calculate Scenic Score.vi" Type="VI" URL="../Day 08/Day 08 - Calculate Scenic Score.vi"/>
			<Item Name="Day 08 - Find Visible Trees.vi" Type="VI" URL="../Day 08/Day 08 - Find Visible Trees.vi"/>
			<Item Name="Day 08 - Parse Input.vi" Type="VI" URL="../Day 08/Day 08 - Parse Input.vi"/>
			<Item Name="Day 08.vi" Type="VI" URL="../Day 08/Day 08.vi"/>
		</Item>
		<Item Name="Day 09" Type="Folder">
			<Item Name="Day 09 - Increment Tail Count.vi" Type="VI" URL="../Day 09/Day 09 - Increment Tail Count.vi"/>
			<Item Name="Day 09 - Move All Knots.vi" Type="VI" URL="../Day 09/Day 09 - Move All Knots.vi"/>
			<Item Name="Day 09 - Move Single Knot.vi" Type="VI" URL="../Day 09/Day 09 - Move Single Knot.vi"/>
			<Item Name="Day 09.vi" Type="VI" URL="../Day 09/Day 09.vi"/>
		</Item>
		<Item Name="Day 10" Type="Folder">
			<Item Name="Day 10 - Get Cycle X Value.vi" Type="VI" URL="../Day 10/Day 10 - Get Cycle X Value.vi"/>
			<Item Name="Day 10.vi" Type="VI" URL="../Day 10/Day 10.vi"/>
		</Item>
		<Item Name="Day 11" Type="Folder">
			<Item Name="Monkeys" Type="Folder">
				<Item Name="Examples" Type="Folder">
					<Item Name="Monkey - Example 0.lvclass" Type="LVClass" URL="../Day 11/Monkeys/Monkey - Example 0/Monkey - Example 0.lvclass"/>
					<Item Name="Monkey - Example 1.lvclass" Type="LVClass" URL="../Day 11/Monkeys/Monkey - Example 1/Monkey - Example 1.lvclass"/>
					<Item Name="Monkey - Example 2.lvclass" Type="LVClass" URL="../Day 11/Monkeys/Monkey - Example 2/Monkey - Example 2.lvclass"/>
					<Item Name="Monkey - Example 3.lvclass" Type="LVClass" URL="../Day 11/Monkeys/Monkey - Example 3/Monkey - Example 3.lvclass"/>
				</Item>
				<Item Name="Monkey 0.lvclass" Type="LVClass" URL="../Day 11/Monkeys/Monkey 0/Monkey 0.lvclass"/>
				<Item Name="Monkey 1.lvclass" Type="LVClass" URL="../Day 11/Monkeys/Monkey 1/Monkey 1.lvclass"/>
				<Item Name="Monkey 2.lvclass" Type="LVClass" URL="../Day 11/Monkeys/Monkey 2/Monkey 2.lvclass"/>
				<Item Name="Monkey 3.lvclass" Type="LVClass" URL="../Day 11/Monkeys/Monkey 3/Monkey 3.lvclass"/>
				<Item Name="Monkey 4.lvclass" Type="LVClass" URL="../Day 11/Monkeys/Monkey 4/Monkey 4.lvclass"/>
				<Item Name="Monkey 5.lvclass" Type="LVClass" URL="../Day 11/Monkeys/Monkey 5/Monkey 5.lvclass"/>
				<Item Name="Monkey 6.lvclass" Type="LVClass" URL="../Day 11/Monkeys/Monkey 6/Monkey 6.lvclass"/>
				<Item Name="Monkey 7.lvclass" Type="LVClass" URL="../Day 11/Monkeys/Monkey 7/Monkey 7.lvclass"/>
				<Item Name="Monkey Base.lvclass" Type="LVClass" URL="../Day 11/Monkeys/Monkey Base/Monkey Base.lvclass"/>
			</Item>
			<Item Name="Day 11 - Sort Items.vi" Type="VI" URL="../Day 11/Day 11 - Sort Items.vi"/>
			<Item Name="Day 11.vi" Type="VI" URL="../Day 11/Day 11.vi"/>
		</Item>
		<Item Name="Day 12" Type="Folder">
			<Item Name="Day 12 - All Moves.vi" Type="VI" URL="../Day 12/Day 12 - All Moves.vi"/>
			<Item Name="Day 12 - Allowable Moves.vi" Type="VI" URL="../Day 12/Day 12 - Allowable Moves.vi"/>
			<Item Name="Day 12 - Parse Input.vi" Type="VI" URL="../Day 12/Day 12 - Parse Input.vi"/>
			<Item Name="Day 12.vi" Type="VI" URL="../Day 12/Day 12.vi"/>
		</Item>
		<Item Name="Day 13" Type="Folder">
			<Item Name="Day 13.vi" Type="VI" URL="../Day 13/Day 13.vi"/>
		</Item>
		<Item Name="Day 14" Type="Folder">
			<Item Name="Day 14.vi" Type="VI" URL="../Day 14/Day 14.vi"/>
		</Item>
		<Item Name="Day 15" Type="Folder">
			<Item Name="Day 15.vi" Type="VI" URL="../Day 15/Day 15.vi"/>
		</Item>
		<Item Name="Day 16" Type="Folder"/>
		<Item Name="Day 17" Type="Folder"/>
		<Item Name="Day 18" Type="Folder"/>
		<Item Name="Day 19" Type="Folder"/>
		<Item Name="Day 20" Type="Folder"/>
		<Item Name="Day 21" Type="Folder"/>
		<Item Name="Day 22" Type="Folder"/>
		<Item Name="Day 23" Type="Folder"/>
		<Item Name="Day 24" Type="Folder"/>
		<Item Name="Day 25" Type="Folder"/>
		<Item Name="General SubVIs" Type="Folder">
			<Item Name="Create Permutations.vi" Type="VI" URL="../SubVIs/Create Permutations.vi"/>
			<Item Name="String to 2D Integer Array.vim" Type="VI" URL="../SubVIs/String to 2D Integer Array.vim"/>
			<Item Name="Surrounding Coordinates.vi" Type="VI" URL="../SubVIs/Surrounding Coordinates.vi"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Convert Array to Set__vipm_lv_collection_ext.vim" Type="VI" URL="/&lt;vilib&gt;/VIPM/Collection_Extensions/Set/Convert Array to Set__vipm_lv_collection_ext.vim"/>
				<Item Name="Convert Set to Array__vipm_lv_collection_ext.vim" Type="VI" URL="/&lt;vilib&gt;/VIPM/Collection_Extensions/Set/Convert Set to Array__vipm_lv_collection_ext.vim"/>
				<Item Name="Create Empty Set__vipm_lv_collection_ext.vim" Type="VI" URL="/&lt;vilib&gt;/VIPM/Collection_Extensions/Set/Create Empty Set__vipm_lv_collection_ext.vim"/>
				<Item Name="Delimited String to 1D String Array.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Delimited String to 1D String Array.vi"/>
				<Item Name="Less Comparable.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Comparison/Less/Less Comparable/Less Comparable.lvclass"/>
				<Item Name="Less Functor.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Comparison/Less/Less Functor/Less Functor.lvclass"/>
				<Item Name="Less.vim" Type="VI" URL="/&lt;vilib&gt;/Comparison/Less.vim"/>
				<Item Name="Set Intersection.vim" Type="VI" URL="/&lt;vilib&gt;/set operations/Set Intersection.vim"/>
				<Item Name="Sort 1D Array Core.vim" Type="VI" URL="/&lt;vilib&gt;/Array/Helpers/Sort 1D Array Core.vim"/>
				<Item Name="Sort 1D Array.vim" Type="VI" URL="/&lt;vilib&gt;/Array/Sort 1D Array.vim"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
